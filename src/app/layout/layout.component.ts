import {Component, OnInit} from '@angular/core';
import {FormControls} from "../base";

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})

export class LayoutComponent implements OnInit {
  // FakeData
  DATA: FormControls[] = [
    {
      group: 'form_1',
      name: 'firstName',
      label: 'First name',
      value: 'Mahmoud',
      type: 'text',
      order: 1,
      validators: {
        minLength: 3,
        maxLength: 9,
        required: true
      }
    },
    {
      group: 'form_1',
      name: 'lastName',
      label: 'Last name',
      value: 'Bekheet',
      type: 'text',
      order: 2,
      validators: {
        required: true
      }
    },
    {
      group: 'form_2',
      name: 'agreeTerms',
      label: 'Do you agree?',
      value: false,
      type: 'checkbox',
      order: 1,
      validators: {}
    },
    {
      group: 'form_2',
      name: 'lightDark',
      label: 'Toggle dark mode',
      value: false,
      type: 'toggle',
      order: 2,
      validators: {}
    },
    {
      group: 'form_2',
      name: 'roles',
      label: 'Choose role',
      value: '0',
      type: 'select',
      options: [
        {value: '0', text: 'User'},
        {value: '1', text: 'Admin'},
      ],
      order: 3,
      validators: {
        required: true
      }
    }
  ]
  displayFormGroup = ''
  listForms: string[] = []

  constructor() {
  }

  ngOnInit(): void {
    // Get keys FormGroup
    this.listForms = [...new Set(this.DATA.map(i => i.group))];

    // Display first form
    if (this.listForms.length > 0)
      this.displayFormGroup = this.listForms[0];
  }

  switchForm(groupName: string) {
    this.displayFormGroup = groupName;
  }

}
