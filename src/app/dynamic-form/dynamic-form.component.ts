import {Component, HostListener, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {FormControls} from "../base";

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})

export class DynamicFormComponent implements OnInit {
  @Input() DATA: FormControls | any;
  @Input() displayFormGroup: any;

  @HostListener('window:keydown', ['$event'])
  // Hotkey shortcuts
  onKeyDown($event: any) {
    switch ($event.keyCode) {
      case 112: // F1
        $event.preventDefault(); // Disable browser event F1
        this.onSubmit();
        break;
      case 113: // F2
        $event.preventDefault(); // Disable browser event F2
        this.form.reset();
        break;
    }
  }

  payLoad = '';

  form: FormGroup = this.formBuilder.group({});

  get isInValid() {
    return this.form.controls;
  }

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    // Merge group by name
    const x = this.groupBy(this.DATA, 'group');

    // Initialize formGroup
    this.createFormGroup(x);
  }

  /*
* Sort by order
* Group an array of objects by a key
* */
  groupBy(arr: any[], key: string) {
    arr.sort((a: any, b: any) => a.order - b.order)
    return arr.reduce((acc, item) => ((acc[item[key]] = [...(acc[item[key]] || []), item]), acc), {})
  };

  // Create a FormControl and return FormGroup
  createFormControl(controls: FormControls[]): FormGroup {
    const group: any = {};

    for (const control of controls) {
      const validatorsToAdd: any[] = [];

      // Append validators to FormControl
      for (const [key, value] of Object.entries(<object>control.validators)) {
        switch (key) {
          case 'required':
            if (value) validatorsToAdd.push(Validators.required);
            break;
          case 'minLength':
            validatorsToAdd.push(Validators.minLength(value));
            break;
          case 'maxLength':
            validatorsToAdd.push(Validators.maxLength(value));
            break;
          default:
            break;
        }
      }

      // Append FormControl to FormGroup
      group[control.name] = new FormControl(control.value, validatorsToAdd);
    }

    // @return FormGroup
    return this.formBuilder.group(group)
  }

  // Create a FormGroup and push it to the form
  createFormGroup(group: FormControls[] | any[]): void {
    for (const [key, value] of Object.entries(group)) {
      // Add FormGroup to Form
      this.form.addControl(
        key,
        this.createFormControl(value)
      );
    }
  }

  onSubmit() {
    this.payLoad = JSON.stringify(this.form.getRawValue(), null, 4);
    console.log('Form valid: ', this.form.valid);
    console.log('Form values: ', this.form.value);
  }

}
