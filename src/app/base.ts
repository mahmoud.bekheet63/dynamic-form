interface FormValidators {
  required?: boolean;
  minLength?: number;
  maxLength?: number;
}

interface FormControlOptions {
  value?: string;
  text?: string;
}

export interface FormControls {
  name: string;
  label: string;
  value: string | number | boolean;
  type: string;
  order: number;
  options?: FormControlOptions | object;
  validators?: FormValidators;
  group: string;
}
export interface JsonFormData {
  controls: FormControls[];
}
